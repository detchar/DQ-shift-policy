% ODC sub-system description template
%
% Author: Duncan Macleod <duncan.macleod@ligo.org>
%
% This document should be copied and edited to provide a technical document
% for all ODC definitions, ideally one per interferometer sub-system.
% The author should read all instructions marked 'XXX' so as to complete the
% document in a manner that is consistent with all of the other ODC subsystem
% descriptions.

\documentclass[
    colorlinks=true,
    pdfstartview=FitV,
    linkcolor=blue,
    citecolor=magenta,
    urlcolor=blue,
]{ligodoc}

\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{acronym}
\usepackage[small,margin={20px,10px},hang]{caption}
\usepackage{rotating}

% -- acronyms -----------------------------------------------------------------
\newacro{ODC}[Online Detector Characterization]{ODC}

% -- custom definitions -------------------------------------------------------
\newcommand\hl[1]{\textcolor{red}{#1}}
\newcommand\bitmaskyes{\textcolor{green}{\checkmark}}
\newcommand\bitmaskno{\textcolor{red}{\ensuremath{\times}}}
\newcommand\channel[1]{\begin{quote}\texttt{#1}\end{quote}}
\newcommand\abs[1]{\left|\,#1\,\right|}

\title{Data Quality Shift Policy Update}
\author{Beverly Berger for the LIGO Detector Characterization Group}
\ligodccnumber{L}{1500110}{}
\ligodistribution{LSC}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}
Data Quality (DQ) shifts are an excellent method for tracking the quality of 
the data from the LIGO interferometers, both from an instrumentation and a 
search point of view. By conducting these shifts rapid feedback can be 
given to commissioners on-site if something detrimental to the data quality 
occurs, thereby reducing the amount of sub-optimal data. In addition, by 
constantly studying the output of the detectors, DQ shifts will play a central 
role in the confidence of a detection. These shifts provided vital feedback in the 
first observing run and in the detection validation procedure of both GW150914 
and GW151226.
\\\\
DQ shifts are studies of 7 days duration of data from one 
of the LIGO interferometers undertaken by a designated individual. The 
individual is expected to study the output of one detector every day to 
look for data quality changes which may adversely affect a gravitational 
wave search or indicate an instrumental malfunction. Daily studies of the interferometer are to promote rapid 
feedback with commissioners should a significant change occur. If LIGO Fellows are available, the designated DQ shifter shall communicate daily with the fellow(s) designated for that shift. If fellows are not available, the site operations manager should designate someone at the site to fill this role.
The DQ shifter will document their daily studies in a `DQ Shift Report' which 
will be presented to the Detector Characterization (DetChar) Group as soon as possible after the end of the shift. Finally the 
DQ shifter is to summarize their findings and post an aLOG entry to the relevant site.

\section{Details}
The most up to date information about DQ shifts can be found on a subsection 
of the data quality wiki: (COMMENT: THIS WIKI NEEDS TO BE UPDATED FOR O4.)
\\\\
\url{https://wiki.ligo.org/DetChar/O4_DQ_Shifts}
Note: This needs to be updated before the final version of this document is complete.
\\\\
This area should be regularly maintained to include links to DQ shift training 
material, DQ Shift instructions (i.e. how to conduct a DQ shift), links to 
recently conducted and upcoming DQ shifts, and a list of current DQ volunteers. 
%(links to these areas can be found in the section \ref{sec:docs}). 
DQ shift volunteers must go through a period of training before conducting 
their own shift 
%(explained in section \ref{sec:volunteer}). This is 
to ensure 
that each shift is conducted consistently and to a high standard. Not only will 
this aid detector characterization operations as a whole, but this will also ease the 
validation process when there is a candidate gravitational wave event.
\\\\
Each DQ shift will focus on one interferometer only. The main resources which 
will be used in a DQ shift are the LIGO summary pages and aLOGs (remember the 
summary pages are given in UTC and the aLOGs in a confusing mixture of local and UTC time):
\begin{itemize}
\item LHO
\begin{itemize}
\item \url{https://ldas-jobs.ligo-wa.caltech.edu/~detchar/summary/}
\item \url{https://alog.ligo-wa.caltech.edu/aLOG/}
\end{itemize}
\item LLO
\begin{itemize}
\item \url{https://ldas-jobs.ligo-la.caltech.edu/~detchar/summary/}
\item \url{https://alog.ligo-la.caltech.edu/aLOG/}
\end{itemize}
\end{itemize}
DQ shift volunteers will consult these resources and document their findings 
on a separate wiki page (all DQ shift wiki pages are obviously named - this 
will become clear before a volunteer conducts their own shift). To avoid confusion and to maintain consistency that allows the data quality report (DQR) to automatically find the appropriate wiki page, the DQ shift coordinator should construct the wiki page for each shift.
A volunteer will use this wiki page 
as their report which they will present to DetChar in 5-10 minutes on a 
scheduled telecon. The content of the aLOG (about 6 bullet points in length) 
will also be taken from this report (which should be linked) and 
posted to the relevant site.
\\\\
LSC Fellows will be on hand to assist DQ shift volunteers should they have 
questions about on-site activities. In addition the fellows will also help in 
conducting long term investigations should interesting features occur in a DQ 
shift. During an observing run, it is expected that DQ shift volunteers 
communicate their daily findings to LSC Fellows on site, so that relevant 
information can be communicated to the commissioners relatively quickly. 
Therefore a daily telecon will be set up between at least one fellow and the 
DQ shift volunteer.
\\\\
An example timeline outlining the cadence of each DQ shift, the presentation 
of the report and the latest day to write an aLOG is given in Table 
\ref{table:timeline}. DQ shifts are typically conducted between UTC days: 
Monday 00:00 UTC through Sunday 23:59 UTC.

\begin{sidewaystable}
%\begin{table}[!htbp]
\centering
\begin{tabular}{|c|c|c|c|c|c|c|c|}
\hline
 & Monday & Tuesday & Wednesday & Thursday & Friday & Saturday & Sunday \\ 
\hline
Week 1 & \multicolumn{3}{|c|}{DQ Shift 1} & \multicolumn{4}{|c|}{DQ Shift 2} \\
 & & & & DQ Shift 1 & & & \\
 & & & & Presentation & & & \\
 & & & & (TS: 12pm Central) & & & \\
 & & & & aLOG no later & & & \\
 & & & & than this date & & & \\
\hline
Week 2 & \multicolumn{3}{|c|}{DQ Shift 3} & \multicolumn{4}{|c|}{DQ Shift 4} \\
 & DQ Shift 2 & & & DQ Shift 3 & & & \\
 & Presentation & & & Presentation & & & \\
 & (TS: 11am Central) & & & (TS: 12pm Central) & & & \\
 & aLOG no later & & & aLOG no later & & & \\
 & than this date & & & than this date & & & \\
\hline 
\end{tabular}
\caption{\label{table:timeline}A two week example timeline outlining the 
format of DQ Shifts for a given detector. DQ Shifts are typically conducted 
between UTC days. DQ Shifts are always presented on TeamSpeak (TS) 
in the DetChar channel during a DetChar telecon. The time is specified in the 
table. Also specified is the latest date the DQ shift volunteer must post an 
aLOG by summarizing their findings.}
%\end{table}
\end{sidewaystable}

\section{DQ Shift Coordinator(s)}\label{sec:coordinator}
The are DQ Shift Coordinator, who is tasked with ensuring the smooth 
running of the 
DQ Shifts both inside and outside an observing run. The coordinator will help 
to/find a mentor when new volunteers come forward and also help to/find a 
mentor for those who need assistance when conducting their shift. As of 
January 2022 the DQ shift coordinator is
(\href{mailto:beverly.berger@ligo.org}{beverly.berger@ligo.org}) at Stanford).

\section{Procedure for becoming a DQ Shift Volunteer}\label{sec:volunteer}
New people are welcome to start training to become a DQ Shift volunteer at any 
time. 
%It is required however that each person who undergoes training commits to 
%being available to undertake DQ shifts for the next calendar year. This will 
%likely entail 3-6 shifts. If an individual cannot commit to this length of time 
%please contact DetChar for other projects you can become involved with.\\\\
DetChar is asking all new DQ shift volunteers to undergo a period of training 
before they conduct their own shift, so that each DQ shift is completed to the 
same standard to the extent possible. If you would like to become a volunteer %(and can commit to the 
%time period) 
please contact the DQ shift coordinator (section 
\ref{sec:coordinator}) so that you may be added to the list of DQ shift 
volunteers in training. Next start your initial training:

\begin{itemize}
\item Attend the main DetChar Telecons (Monday 9 am Pacific on TeamSpeak). This 
is to give the volunteer an idea of what is generally happening in DetChar. We 
ask you to attend as regularly as possible.
\item Become familiar with the Summary Pages as this is one of the main 
resources you will be using in your DQ shifts. Training documents and telecons 
will become available soon  (DCC: TBD). UPDATE REQUIRED
\item Read all the current documentation about DQ shifts. These are the links 
given in section \ref{sec:docs}. UPDATE REQUIRED
\item Redo a DQ shift that has already been conducted and compare your findings 
with the original.
\end{itemize}
After all these items have been completed please contact the DQ shift 
coordinator to indicate  that you have completed the initial training. The 
next step is about gaining experience. To begin with, the new volunteer will 
shadow another volunteer who is conducting a shift. The DQ shift coordinator 
will work with the new volunteer in finding an appropriate person to shadow. 
Once this is completed the new volunteer will work with the DQ shift 
coordinator to sign up for and find a suitable mentor who will assist them in 
their upcoming DQ shift.\\\\
Anyone who has not conducted a DQ shift in O3 must complete this training 
before conducting a DQ shift on their own. The DQ shift coordinator will assess when a DQ shift volunteer does not 
need a mentor to conduct their shift. 
Remember though that the DQ shift coordinator and DetChar are available to 
help with questions when a DQ shift volunteer is on shift.

\section{Assigning DQ Shifts}
Outside of observing runs the DQ shift coordinator is responsible for 
assigning 
DQ shifts as the detectors lock or during an engineering run. This will be done 
by cycling through a list of trained/in-training volunteers and emailing each 
in turn to check if they are available to conduct a DQ shift.\\\\ 
During an observing run the DQ shift coordinator will call for trained DQ 
shift volunteers to sign up for DQ shifts on the relevant wiki page. For O4 
this is
\\\\
\url{https://wiki.ligo.org/DetChar/O4_DQ_Shifts}. MAY NEED UPDATE.
\\\\
Here trained volunteers will be able to sign up for DQ shifts of their 
choosing, and the DQ shift coordinators will maintain this page. Do remember 
that only trained volunteers may sign themselves up. Should a DQ shift 
volunteer in training wish to conduct a shift they must coordinate with the DQ 
shift coordinators as a mentor will be required.

\section{Documentation}\label{sec:docs} UPDATE REQUIRED
\begin{itemize}
\item DQ shift instructions: \url{https://wiki.ligo.org/DetChar/DataQuality/ShiftInstructions}
\item DetChar FAQ: \url{https://dcc.ligo.org/LIGO-G1400062}
\item How to read the summary pages: \url{https://dcc.ligo.org/LIGO-G1401357}
\item DQ shift training telecons: \url{https://dcc.ligo.org/LIGO-G1500145}
\end{itemize}

\section{Useful Links} UPDATE REQUIRED
\begin{itemize}
\item GW150914 DetChar paper: \url{http://arxiv.org/abs/1602.03844}
\item Pre O1 DetChar work: \url{http://arxiv.org/abs/1508.07316}
\item S6 LIGO DetChar Paper: \url{http://arxiv.org/abs/1410.7764}
\item DetChar wiki: \url{https://wiki.ligo.org/DetChar}
\item Detchar Tools wiki: \url{https://wiki.ligo.org/DetChar/Tools}
\item Past and Present DQ Shifts:
\begin{itemize}
\item S6: \url{https://wiki.ligo.org/DetChar/GlitchStudies}
\item ER6: \url{https://wiki.ligo.org/DetChar/DataQuality/ER6DataQualityShifts}
\item Pre O1 (February 2015 - September 2015): \url{https://wiki.ligo.org/DetChar/DataQuality/PreO1DQShifts}
\item O1: \url{https://wiki.ligo.org/DetChar/DataQuality/O1DQShifts}
\item ER10/O1: \url{https://wiki.ligo.org/DetChar/DataQuality/O2DQShifts}
\end{itemize}
\item Current Volunteers: \url{https://wiki.ligo.org/DetChar/DataQuality/DQShiftVolunteers}
\item Indrouction to aLIGO for DQ Shift Volunteers: \url{https://dcc.ligo.org/LIGO-G1500980}
\end{itemize}

\section{Contacts}
DQ Shift Coordinators: Beverly Berger (\href{beverly.berger@ligo.org}{beverly.berger@ligo.org}).\\\\
DetChar Chairs: Brennan Hughey (\href{brennan.hughey@ligo.org}{brennan.hughey@ligo.org}) and Derek Davis (\href{mailto:derek.davis@ligo.org}{dderek.davis@ligo.org})
\\\\
Detchar: (\href{mailto:detchar@ligo.org}{detchar@ligo.org})

\end{document}
